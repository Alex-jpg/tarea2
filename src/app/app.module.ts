import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PortadaComponent } from './componentes/portada/portada.component';
import { AcerdadeComponent } from './componentes/acerdade/acerdade.component';
import { GeneradorComponent } from './componentes/generador/generador.component';
import {MatTableModule} from '@angular/material/table';

@NgModule({
  declarations: [
    AppComponent,
    PortadaComponent,
    AcerdadeComponent,
    GeneradorComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatTableModule
 
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
