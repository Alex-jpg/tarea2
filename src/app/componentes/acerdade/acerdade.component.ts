import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-acerdade',
  templateUrl: './acerdade.component.html',
  styleUrls: ['./acerdade.component.scss']
})
export class AcerdadeComponent implements OnInit {

   title="ACERCA DE";

  constructor() { }

  ngOnInit(): void {
  }

}
