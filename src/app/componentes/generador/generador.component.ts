import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTable } from '@angular/material/table';
import Swal from 'sweetalert2';

interface Tabla1 {
  position: number;
  name: string;
}

interface Tabla2 {
  position: number;
  actividad: string;
  hora: number;
}

interface Tabla3 {
  position: number;
  name: string;
  Lunes: string;
  Martes: string;
  Miercoles: string;
  Jueves: string;
  Viernes: string;
  Sabado: string;
}

@Component({
  selector: 'app-generador',
  templateUrl: './generador.component.html',
  styleUrls: ['./generador.component.scss'],
})
export class GeneradorComponent implements OnInit {
  @ViewChild('tabla2') table: any;

  title = 'GENERADOR ALEATORIO';
  displayedColumns: string[] = ['position', 'name'];
  displayedColumns2: string[] = ['position', 'name', 'hora'];
  displayedColumns3: string[] = [
    'position',
    'name',
    'lunes',
    'martes',
    'miercoles',
    'jueves',
    'viernes',
    'sabado',
  ];

  ELEMENT_DATA: Tabla1[] = [
    { position: 1, name: 'Alexandr' },
    { position: 2, name: 'Aldo' },
    { position: 3, name: 'Carlos' },
    { position: 4, name: 'Eder' },
    { position: 5, name: 'Yoaldi' },
  ];

  ELEMENT_DATA2: Tabla2[] = [
    { position: 1, actividad: 'Barrer', hora: 1 },
    { position: 2, actividad: 'Trapear', hora: 1 },
    { position: 3, actividad: 'Limpiar mostrador', hora: 1 },
    { position: 4, actividad: 'Limpiar productos', hora: 2 },
    { position: 5, actividad: 'Corte de caja', hora: 1 },
    { position: 6, actividad: 'Regar plantas', hora: 1 },
    { position: 7, actividad: 'Limpieza de Laptop', hora: 2 },
    { position: 8, actividad: 'Formateo', hora: 3 },
    { position: 9, actividad: 'Diseño de plano', hora: 5 },
    { position: 10, actividad: 'Atender clientes', hora: 3 },
    { position: 11, actividad: 'Admin. Redes sociales', hora: 3 },
    { position: 12, actividad: 'Registrar ventas', hora: 2 },
    { position: 13, actividad: 'Activar Chip', hora: 3 },
    { position: 14, actividad: 'Cotizar piezas', hora: 3 },
    { position: 15, actividad: 'Dar asesoria tecnica', hora: 4 },
    { position: 16, actividad: 'Pedir mercancia', hora: 2 },
    { position: 17, actividad: 'Revisar mercancia', hora: 3 },
    { position: 18, actividad: 'Atualizar SO', hora: 2 },
    { position: 19, actividad: 'Tiempo Completo', hora: 8 },
    { position: 20, actividad: 'Ponchar cables de red', hora: 3 },
    { position: 21, actividad: 'Contratatos de servicios ', hora: 1 },
    { position: 22, actividad: 'Etiquetar precios', hora: 3 },
    { position: 23, actividad: 'Ordenar mercancia', hora: 2 },
    { position: 24, actividad: 'Registro de gastos', hora: 1 },
    { position: 25, actividad: 'Publicidad ', hora: 3 },
    { position: 26, actividad: 'Comprar comida', hora: 2 },
    { position: 27, actividad: 'Descanso', hora: 2 },
    { position: 28, actividad: 'Comer', hora: 1 },
    { position: 29, actividad: 'Entregar equipos ', hora: 1 },
    { position: 30, actividad: 'Reparar equipos ', hora: 1 },
  ];
  ELEMENT_DATA3: Tabla3[] = [
    {
      position: 1,
      name: '',
      Lunes: '',
      Martes: '',
      Miercoles: '',
      Jueves: '',
      Viernes: '',
      Sabado: '',
    },
    {
      position: 2,
      name: '',
      Lunes: '',
      Martes: '',
      Miercoles: '',
      Jueves: '',
      Viernes: '',
      Sabado: '',
    },
    {
      position: 3,
      name: '',
      Lunes: '',
      Martes: '',
      Miercoles: '',
      Jueves: '',
      Viernes: '',
      Sabado: '',
    },
    {
      position: 4,
      name: '',
      Lunes: '',
      Martes: '',
      Miercoles: '',
      Jueves: '',
      Viernes: '',
      Sabado: '',
    },
    {
      position: 5,
      name: '',
      Lunes: '',
      Martes: '',
      Miercoles: '',
      Jueves: '',
      Viernes: '',
      Sabado: '',
    },
  ];

  dataSource = this.ELEMENT_DATA;
  dataSource2 = this.ELEMENT_DATA2;
  dataSource3 = this.ELEMENT_DATA3;

  constructor() {}

  ngOnInit(): void {}

  generador() {
    this.dataSource3.forEach((elm) => {
      elm.name = '';
      elm.Lunes = '';
      elm.Martes = '';
      elm.Miercoles = '';
      elm.Jueves = '';
      elm.Viernes = '';
      elm.Sabado = '';
    });

    let iteraciones = Math.floor(Math.random() * (100 - 2 + 1) + 2);
    let listaActividades: Tabla2[] = [];
    this.ELEMENT_DATA2.forEach((act) => {
      listaActividades.push(act);
    });

    this.dataSource.forEach((elemento) => {
      let posicion = 0;
      for (let i = 0; i < iteraciones; i++) {
        posicion = Math.floor(
          Math.random() * (this.dataSource.length - 1 - 0 + 1) + 0
        );
      }

      let b = true;
      do {
        b = true;
        if (this.dataSource3[posicion].name === '') {
          this.dataSource3[posicion].name = elemento.name;

          for (let dia = 1; dia < 7; dia++) {
            
            let actividades: string = '';
            let count: number = 0;
            let ocho = true;
            do {
              ocho = true;
              let pos = Math.floor(
                Math.random() * (listaActividades.length - 1) + 1
              );
              let nuevo: Tabla2 = this.obtener(listaActividades, pos);

              if (nuevo.actividad != '' && count + nuevo.hora <= 8) {
                count += nuevo.hora;

                actividades += nuevo.actividad + ' [' + nuevo.hora + 'hrs] - ';
                listaActividades = this.nuevaLista(nuevo.position,listaActividades);
                if (count == 8) {
                  ocho = false;
                }
              }
            } while (ocho);

            
            
            if (dia == 1) {
              this.dataSource3[posicion].Lunes = actividades.slice(0, -2);
            } else if (dia == 2) {
              this.dataSource3[posicion].Martes = actividades.slice(0, -2);
            } else if (dia == 3) {
              this.dataSource3[posicion].Miercoles = actividades.slice(0, -2);
            } else if (dia == 4) {
              this.dataSource3[posicion].Jueves = actividades.slice(0, -2);
            } else if (dia == 5) {
              this.dataSource3[posicion].Viernes = actividades.slice(0, -2);
            } else if (dia == 6) {
              this.dataSource3[posicion].Sabado = actividades.slice(0, -2);
            }
            b = false;
            
            listaActividades = this.ELEMENT_DATA2;
          }
        }
        posicion = Math.floor(
          Math.random() * (this.dataSource.length - 1 - 0 + 1) + 0
        );
      } while (b == true);
    });
    this.table.renderRows();
    Swal.fire({
      icon: 'success',
      text: 'Se generaron ' + iteraciones + ' iteraciones',
      width: '20%',
      padding: '5px',
    });
  }

  obtener(listaActividades: Tabla2[], pos: number): any {
    let elemento: Tabla2 = {
      actividad: '',
      position: 1,
      hora: 0,
    };
    listaActividades.forEach((el) => {
      if (el.position === listaActividades[pos].position) {
        elemento.position = el.position;
        elemento.actividad = el.actividad;
        elemento.hora = el.hora;
      }
    });
    return elemento;
  }

  nuevaLista(id: number, listaActividades: Tabla2[]): Tabla2[] {
    let tabla: Tabla2[] = listaActividades;
    listaActividades = [];
    tabla.forEach((act) => {
      if (act.position != id) {
        listaActividades.push(act);
      }
    });

    return listaActividades;
  }

  generador2() {
    this.dataSource3.forEach((elm) => {
      elm.name = '';
      elm.Lunes = '';
      elm.Martes = '';
      elm.Miercoles = '';
      elm.Jueves = '';
      elm.Viernes = '';
      elm.Sabado = '';
    });

    let iteraciones = Math.floor(Math.random() * (100 - 2 + 1) + 2);
    let listaActividades: Tabla2[] = [];
    this.ELEMENT_DATA2.forEach((act) => {
      listaActividades.push(act);
    });

    this.dataSource.forEach((elemento) => {
      let posicion = 0;
      for (let i = 0; i < iteraciones; i++) {
        posicion = Math.floor(
          Math.random() * (this.dataSource.length - 1 - 0 + 1) + 0
        );
      }

      let b = true;
      do {
        b = true;
        if (this.dataSource3[posicion].name === '') {
          this.dataSource3[posicion].name = elemento.name;

          let actividades: string = '';
          let count: number = 0;
          let ocho = true;
          do {
            ocho = true;
            let pos = Math.floor(
              Math.random() * (listaActividades.length - 1) + 1
            );
            let nuevo: Tabla2 = this.obtener(listaActividades, pos);

            if (nuevo.actividad != '' && count + nuevo.hora <= 8) {
              count += nuevo.hora;

              actividades += nuevo.actividad + ' [' + nuevo.hora + 'hrs], ';
              listaActividades = this.nuevaLista(
                nuevo.position,
                listaActividades
              );
              if (count == 8) {
                ocho = false;
              }
            }
          } while (ocho);

          this.dataSource3[posicion].Lunes = actividades.slice(0, -2);
          b = false;
        }
        posicion = Math.floor(
          Math.random() * (this.dataSource.length - 1 - 0 + 1) + 0
        );
      } while (b == true);
    });
    this.table.renderRows();
    Swal.fire({
      icon: 'success',
      text: 'Se generaron ' + iteraciones + ' iteraciones',
      width: '20%',
      padding: '5px',
    });
  }

  /*

  


  generador() {
    this.dataSource3.forEach(elm => {
      elm.name='';
      elm.Lunes = '';
      elm.Martes = '';
      elm.Miercoles = '';
      elm.Jueves = '';
      elm.Viernes = '';
      elm.Sabado = '';
    });

    let iteraciones = Math.floor((Math.random() * (100 - 2 + 1)) + 2);
    let listaActividades: Tabla2[] = [];
    this.ELEMENT_DATA2.forEach(act => {
      listaActividades.push(act);
    });
    this.dataSource.forEach(elemento => {      
      let posicion = 0;
      for (let i = 0; i < iteraciones; i++) {
        posicion = Math.floor((Math.random() * ((this.dataSource.length - 1) - 0 + 1)) + 0);
      }
      
      let b = true;
      do{
        b = true;
        if (this.dataSource3[posicion].name === '') {
          this.dataSource3[posicion].name = elemento.name;

          let actividades: string="";
          let count : number = 0;
          let totalHoras: string = "";
          let ocho = true;
          do{
            ocho = true;
            let pos = Math.floor((Math.random() * (listaActividades.length - 1) + 1));
            let nuevo: Tabla2 = this.obtener(listaActividades, pos);
            
            console.log('pos: '+ pos);
            if (nuevo.actividad != '' && ((count + nuevo.hora) <= 8 ))
            { 
              console.log(nuevo);
              count += nuevo.hora;
              console.log('total: '+count);
              
              totalHoras += nuevo.hora + ", ";
              actividades += nuevo.actividad + ", ";
              console.log(listaActividades);
              listaActividades.splice(this.dataSource3[posicion].position, 1);
              //listaActividades = listaActividades.filter(function(el) { return el.position != pos});
              console.log(listaActividades);
              if(count==8){
                ocho = false;
              }
            }
          } while(ocho);
          console.log('--------'+totalHoras+'-----------');
          
          
          this.dataSource3[posicion].Lunes = totalHoras.slice(0,-2);
          b = false;
        }
        posicion = Math.floor((Math.random() * ((this.dataSource.length - 1) - 0 + 1)) + 0);
      }while(b == true);
    });
    this.table.renderRows();
    Swal.fire({
      icon: 'success',
      text: 'Se generaron ' + iteraciones + ' iteraciones',
      width: '20%',
      padding: '5px' 
      
    })
  }

  obtener(listaActividades: Tabla2[], pos: number): any{
    let elemento: Tabla2 = {
      actividad: '',
      position: 1,
      hora: 0
    };
    listaActividades.forEach(el => {
      if(el.position === pos){
        elemento.position = el.position;
        elemento.actividad = el.actividad;
        elemento.hora = el.hora;
      }
    });
    return elemento;
  }


  generador1() {
    this.dataSource2.forEach(elm => {
      elm.name='';
      elm.actividad='';
    });

    let arr: Array<Number> = [];
    let iteraciones = Math.floor((Math.random() * (100 - 2 + 1)) + 2);
    let listaActividades: PeriodicElement[] = [];
    this.ELEMENT_DATA3.forEach(act => {
      listaActividades.push(act);
    });
    this.dataSource.forEach(elemento => {      
      let posicion = 0;
      for (let i = 0; i < iteraciones; i++) {
        posicion = Math.floor((Math.random() * ((this.dataSource.length - 1) - 0 + 1)) + 0);
      }
      
      let b = true;
      do{
        b = true;
        if (this.dataSource2[posicion].name === '') {
          this.dataSource2[posicion].name = elemento.name;

          let actividades: string="";
          for (let i = 0; i<3; i++ ){
            let pos =Math.floor((Math.random() * ((listaActividades.length-1)-1+0)))
            let nuevo: PeriodicElement[]=listaActividades.splice(pos,1);
            console.log(nuevo);
            
            actividades+=nuevo[0].actividad + ", ";
          }
          
          this.dataSource2[posicion].actividad = actividades.slice(0,-2);
          b = false;
        }
        posicion = Math.floor((Math.random() * ((this.dataSource.length - 1) - 0 + 1)) + 0);
      }while(b == true);
    });
    this.table.renderRows();
    Swal.fire({
      icon: 'success',
      text: 'Se generaron ' + iteraciones + ' iteraciones',
      width: '20%',
      padding: '5px' 
      
    })
  }
  */
}
