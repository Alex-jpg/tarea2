import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AcerdadeComponent } from './componentes/acerdade/acerdade.component';
import { GeneradorComponent } from './componentes/generador/generador.component';
import { PortadaComponent } from './componentes/portada/portada.component';


const routes: Routes = [
  {path:"", component:PortadaComponent},
  {path:"acercade", component:AcerdadeComponent},
  {path:"generador", component:GeneradorComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
